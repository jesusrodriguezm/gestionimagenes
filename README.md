# Gestión de imágenes y documentos con GRAILS (prototipo) #

Prototipo desarrollado para probar la gestión de imágenes y documentos asociados con clases de dominio.

Se ha utilizado como base un nuevo proyecto limpio en GRAILS (2.5.1), al que se ha añadido Bootstrap para mejorar la visualiación de los formularios relativos a imágenes, documentos y vídeos.

El diseño permite añadir con relativamente poco código archivos multimedia a cualquier clase de dominio (se reutilizan las plantillas para las pantallas de inserción y borrado, y se reutiliza los métodos de inserción y borrado en controladores haciendo un forward en el controlador de cada clase de dominio).

A la hora de cargar los ficheros multimedia, se realiza validación en cliente y servidor de tipo (ya no se valida tamaño, se redimensionan las imágenes al subirlas).

# Más información sobre la aplicación #
* [Gestión de imágenes y documentos (prototipo GRAILS)](http://wp.me/p1A5aJ-5s)
* Video demo: [https://youtu.be/cti6Id3CNgI](https://youtu.be/cti6Id3CNgI)