package gestionimagenes

import gestionImagenes.multimedia.iMultimedia
import gestionImagenes.Utiles

class Propiedad implements iMultimedia {		
	
	String nombre
	
	PropiedadImagenPrincipal imagenPrincipal
	
	static hasMany = [imagenes:PropiedadImagen, planos:PropiedadPlano, documentos:PropiedadDocumento, videos:PropiedadVideo]

    static constraints = {
		nombre nullable:false, blank:false
		imagenPrincipal nullable:true, blank:true
		imagenes nullable:true, blank:true
		planos nullable:true, blank:true
		documentos nullable:true, blank:true
		videos nullable:true, blank:true
    }
	
	String toString() {
		return this.nombre
	}
	
	@Override
	boolean puedeAñadirNuevaImagen() {
		return Utiles.obtenerConfig().maximoImagenesPorPropiedad > this.imagenes.size()		
	}
	
	@Override
	boolean puedeAñadirNuevoPlano() {
		return Utiles.obtenerConfig().maximoImagenesPorPropiedad > this.planos.size()
	}
	
	@Override
	boolean puedeAñadirNuevoDocumento() {		
		return Utiles.obtenerConfig().maximoDocumentosPorPropiedad > this.documentos.size()
	}

	@Override
	public boolean puedeAñadirNuevoVideo() {
		//Sin limites
		return true;
	}
}
