package gestionimagenes

import org.springframework.web.multipart.MultipartFile;

import gestionimagenes.multimedia.MultimediaDocumento

class PropiedadDocumento extends MultimediaDocumento {

	static int sizeMax = 10240 //KB --> 10MB
	
	private static String ruta = "propiedad/documentos/"
	
    static belongsTo = [propiedad:Propiedad]
	
    static constraints = {
		propiedad nullable:false, blank:false			
    }
	
	static mapping = {
		propiedad fetch: 'join'
	}
	
	def afterDelete() {
		println "afterDelete()"
		super.afterDelete()
	}
	
	PropiedadDocumento(MultipartFile file, Long propiedadId) {		
		this.nombre = file.getOriginalFilename()
		this.tamanoKB = Math.round(file.getSize() / 1024)
		this.propiedad = Propiedad.get(propiedadId)
    }
	
	public String obtenerRuta() {
		return MultimediaDocumento.ruta + PropiedadDocumento.ruta + this.propiedad.id.toString() + "/"
	}
	
	public String getTipo() {
		return "Documento propiedad"
	}
	
	public getContenedor() {
		return this.propiedad
	}

}
