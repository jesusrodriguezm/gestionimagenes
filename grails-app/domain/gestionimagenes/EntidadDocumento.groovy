package gestionimagenes

import gestionimagenes.multimedia.MultimediaDocumento
import org.springframework.web.multipart.MultipartFile;

class EntidadDocumento extends MultimediaDocumento {
	
	static int sizeMax = 10240 //KB --> 10MB
	
	private static String ruta = "entidad/documentos/"
	
	static belongsTo = [entidad:Entidad]

    static constraints = {
		entidad nullable:false, blank:false
    }
	
	static mapping = {
		entidad fetch: 'join'
	}
	
	def afterDelete() {
		println "afterDelete()"
		super.afterDelete()
	}
	
	EntidadDocumento(MultipartFile file, Long entidadId) {
		this.nombre = file.getOriginalFilename()
		this.tamanoKB = Math.round(file.getSize() / 1024)
		this.entidad = Entidad.get(entidadId)
	}
	
	public String obtenerRuta() {
		return MultimediaDocumento.ruta + EntidadDocumento.ruta + this.entidad.id.toString() + "/"
	}
	
	public String getTipo() {
		return "Documento entidad"
	}
	
	public getContenedor() {
		return this.entidad
	}
	
}
