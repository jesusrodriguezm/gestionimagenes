package gestionimagenes

import org.springframework.web.multipart.MultipartFile;

import gestionimagenes.multimedia.MultimediaImagen;

class PropiedadPlano extends MultimediaImagen {

   	//static int sizeMax = 512 //KB
	
	private static String ruta = "propiedad/planos/"
	
	static belongsTo = [propiedad:Propiedad]
	
    static constraints = {
		propiedad nullable:false, blank:false				
    }
	
	static mapping = {
		propiedad fetch: 'join'
	}
	
	def afterDelete() {
		println "afterDelete()"	
		super.afterDelete()
	}
	
	PropiedadPlano(MultipartFile file, Long propiedadId) {
		this.nombre = file.getOriginalFilename()
		this.tamanoKB = Math.round(file.getSize() / 1024)
		this.propiedad = Propiedad.get(propiedadId)
	}
	
	public String obtenerRuta() {
		return MultimediaImagen.ruta + PropiedadPlano.ruta + this.propiedad.id.toString() + "/"
	}
	
	public String getTipo() {
		return "Plano propiedad"
	}
	
	public getContenedor() {
		return this.propiedad
	}

}
