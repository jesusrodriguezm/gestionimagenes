package gestionimagenes.multimedia

abstract class MultimediaVideo extends Multimedia {
	
	String urlVideo

    static constraints = {
		urlVideo nullable:false, blank:false, maxSize:100
    }
	
	public abstract String obtenerRuta()
	
	public abstract String getTipo()
	
	public abstract getContenedor()
	
	public Boolean esYoutube() {
		println "urlVideo: " + urlVideo
		if (urlVideo.contains("//youtu.be") || urlVideo.contains("www.youtube")) {
			println "video youtube"
			return true
		}
		println "no es un video de youtube"
		return false
	}
	
	public Boolean esVimeo() {
		if (urlVideo.contains("//vimeo")) {
			println "video vimeo"
			return true
		}
		println "no es un video de vimeo"
		return false
	}
	
	public Boolean direccionVideoValida() {
		return (this.esYoutube() || this.esVimeo())
	}
	
	
	public String obtenerDireccionEmbeber() {
		def urlVideo = ""
		def identificadorVideo = ""
		if (this.esYoutube()) {
			// de https://youtu.be/4QZvLSGuYoo a https://www.youtube.com/embed/4QZvLSGuYoo
			urlVideo = "https://www.youtube.com/embed/"					
		}
		else {
			if (this.esVimeo()) {
				//de https://vimeo.com/channels/staffpicks/138645198 a https://player.vimeo.com/video/138645198
				urlVideo = "https://player.vimeo.com/video/"
			}
		}
		
		if (urlVideo!="") { //solo si se ha reconocido el formato del vido (youtube o vimeo) 
			def urlCortada = this.urlVideo.split("/")
			identificadorVideo = urlCortada[urlCortada.size() - 1]
			urlVideo = urlVideo + identificadorVideo
		}
		println urlVideo
		
		return urlVideo
	}
	
	
}
