package gestionimagenes.multimedia

abstract class MultimediaDocumento extends Multimedia {

    static transients = ['ruta']

	private static String ruta = ""
	
	String documento
	
	static constraints = {
		documento nullable:true, blank:true		
	}
	
	def afterDelete() {
		//Borrar el fichero del sistema de ficheros
		println "MultimediaImagen - afterDelete()"
		println this.documento
				
		if (this.documento != "") {
			if (!fileUploadService.removeFile(this.documento, this.obtenerRuta())) {
				println "NO SE HA BORRADO EL FICHERO: " + this.obtenerRuta() + this.documento
			}
		}
	}
	
	String toString() {
		return this.documento
	}
	
	public abstract String obtenerRuta()
	
	public abstract String getTipo()
	
	public abstract getContenedor()
	
}
