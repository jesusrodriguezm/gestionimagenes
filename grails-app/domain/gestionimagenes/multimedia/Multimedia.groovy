package gestionimagenes.multimedia

import gestionimagenes.FileUploadService;

abstract class Multimedia {
	
	static transients = ['fileUploadService']

	FileUploadService fileUploadService
	
	String nombre
	double tamanoKB = 0.0
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
		id(display:false, attributes:[listable:false]) // do not show id anywhere
		nombre nullable:true, blank:true, maxSize:200
		tamanoKB nulable:true, blank:true
    }
	
	public abstract String obtenerRuta()
	
	public abstract String getTipo()
	
	public abstract getContenedor()
	
}
