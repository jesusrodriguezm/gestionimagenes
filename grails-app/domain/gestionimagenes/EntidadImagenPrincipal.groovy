package gestionimagenes

import gestionimagenes.multimedia.MultimediaImagen;
import org.springframework.web.multipart.MultipartFile

class EntidadImagenPrincipal extends MultimediaImagen {

	//static int sizeMax = 512 //KB

	private static String ruta = "entidad/principales/"

	static belongsTo = [entidad:Entidad]

	static constraints = {
		entidad nullable:false, blank:false
	}
	
	static mapping = {
		entidad fetch: 'join'
	}

	def afterDelete() {
		println "afterDelete()"
		super.afterDelete()
	}

	EntidadImagenPrincipal(MultipartFile file, Long propiedadId) {
		this.nombre = file.getOriginalFilename()
		this.tamanoKB = Math.round(file.getSize() / 1024)
		this.entidad = Entidad.get(propiedadId)
	}

	public String obtenerRuta() {
		return MultimediaImagen.ruta + EntidadImagenPrincipal.ruta + this.entidad.id.toString() + "/"
	}

	public String getTipo() {
		return "Imagen principal entidad"
	}

	public getContenedor() {
		return this.entidad
	}
}
