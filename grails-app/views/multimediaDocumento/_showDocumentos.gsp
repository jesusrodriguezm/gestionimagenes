<g:if test="${contenedor?.documentos?.size()>0}">
	<div class="panel panel-default">
		<div class="panel-heading">
			<g:message code="multimediaDocumentos.label" default="Documentos" />			
			<span class="badge">${contenedor?.documentos?.size()}</span>
		</div>
		<div class="panel-body text-center">
			<% int j = 1; %>
			<div class="table-responsive">
				<table class="table table-condensed table-striped table-hover">
					<tr>
						<th class="width50"></th>
						<th><g:message code="multimedia.nombre.label" default="Nombre" /></th>
						<th><g:message code="multimedia.tamano.label" default="Tamaño" /></th>		
						<th><g:message code="multimedia.dateCreated.label" default="F. subido" /></th>			
					</tr>
					<g:each in="${contenedor?.documentos?.sort{it.id}}" var="i">
						<tr>
							<td><span class="badge"> ${j} <% j++; %></span></td>
							<td><a
								href="<g:createLink controller='multimediaDocumento' action='viewDocumento' params='[id:"${i.id}"]' />"
								target="_blank"> ${i.nombre}
							</a></td>					
							<td>
								 ${i.tamanoKB}KB
							</td>
							<td>								 
								 <g:formatDate format="yyyy-MM-dd HH:mm:ss a" date="${i.dateCreated}"/>
							</td>															
						</tr>
					</g:each>
				</table>
			</div>
		</div>
	</div>
</g:if>