<g:if test="${contenedor.id}">	
		<div class="panel panel-default" id="panelDocumentos">
			<div class="panel-heading">
				<g:message code="multimediaDocumentos.label" default="Documentos" />
				<span class="badge">${contenedor?.documentos?.size()}</span>
				<a data-toggle="collapse" data-parent="#accordion" href="#addDocumento" class="btn btn-default btn-xs pull-right"
					title="${message(code:'default.add.label', args: [message(code: 'multimediaDocumento.label', default: 'Documento')])}">
					<i class="glyphicon glyphicon-plus"></i>
				</a>
			</div>
			<div class="panel-body">
				<g:if test="${flash.messageDocumento}">					
					<bootstrap:alert class="alert alert-info">${flash.messageDocumento}</bootstrap:alert>
				</g:if>
				
				<div id="addDocumento" class="panel-collapse collapse">
					<g:if test="${contenedor.puedeAñadirNuevoDocumento()}">
						<div class="row">
							<div class="col-md-12">
								<div class="well well-sm">
									<div class="form-group">
										<div class="col-sm-3 control-label">
											<label for="Documento"> ${message(code: 'default.add.label', args: [message(code: 'multimediaDocumento.label', default: 'Documento')])}
											</label>
										</div>
										<div class="col-sm-9">
											<input type="file" name="documentoForm" id="documentoForm"
												accept=".xlsx,.xls,.doc,.docx,.ppt,.pptx,.txt,.pdf"
												onchange="validarFormatoDocumentos(this);validarTamanoMultimedia(this, 10240);$('#documentoFormVisible').val($(this).val().split('\\').pop());"
												class="hidden" />
											
											<div class="input-group">
											 	<span class="input-group-btn">
											    	<a class="btn btn-default" onclick="$('#documentoForm').click();" title="Seleccionar nuevo documento">Sel.</a>
											    </span>
											    <input type="text" name="documentoFormVisible" id="documentoFormVisible" class="form-control" disabled />
											    <span class="input-group-btn">
											    	<g:actionSubmit value="${message(code:'default.add.label', args: [' '])}" 
														action="addDocumento" class="btn btn-primary" title="${message(code:'default.add.label', args: [message(code: 'multimediaDocumento.label', default: 'Documento')])}" />
											    </span>										    
											</div>																									
											<p class="help-block">											
												<g:message code="multimediaDocumentos.ayudaSubida.label" default="Ficheros .xlsx,.xls,.doc,.docx,.ppt,.pptx,.txt o.pdf de un máximo de 10MB" />
											</p>											
										</div>									
									</div>
								</div>
							</div>
						</div>
					</g:if>
					<g:else>
						<div class="well well-sm">
							<g:message code="multimediaDocumentos.maximoAlcanzado.label" default="Máximo de documentos alcanzado" />
						</div>
					</g:else>				
				</div>

			<g:if test="${contenedor?.documentos?.size()>0}">
				<% int j = 1; %>								
				<div class="table-responsive">
					<table class="table table-condensed table-striped table-hover">
						<tr>
							<th class="width50"></th>
							<th><g:message code="multimedia.nombre.label" default="Nombre" /></th>
							<th><g:message code="multimedia.tamano.label" default="Tamaño" /></th>		
							<th><g:message code="multimedia.dateCreated.label" default="F. subido" /></th>
							<th></th>
						</tr>
						<g:each in="${contenedor?.documentos?.sort{it.id}}" var="i">
							<tr>
								<td><span class="badge"> ${j} <% j++; %></span></td>
								<td><a
									href="<g:createLink controller='multimediaDocumento' action='viewDocumento' params='[id:"${i.id}"]' />"
									target="_blank"> ${i.nombre}
									</a>
								</td>
								<td>
								 ${i.tamanoKB}KB
								</td>
								<td>								 
									 <g:formatDate format="yyyy-MM-dd HH:mm:ss a" date="${i.dateCreated}"/>
								</td>
								<td><g:link controller="${i.getClass().simpleName}" action="delete" id="${i.id}"
								class="btn btn-xs btn-danger pull-right"
								onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">
								<span class="glyphicon glyphicon-remove"></span>
					     							Eliminar
					     					</g:link></td>
							</tr>
						</g:each>
					</table>					
				</div>
			</g:if>

		
			</div>
		</div>
</g:if>