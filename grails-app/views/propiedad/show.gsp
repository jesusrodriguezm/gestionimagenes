
<%@ page import="gestionimagenes.Propiedad" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'propiedad.label', default: 'Propiedad')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
		<asset:stylesheet src="jquery-gallery.css"/>
		<asset:javascript src="jquery-gallery.js"/>
	</head>
	<body>
		<a href="#show-propiedad" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-propiedad" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list propiedad">
			
				<g:if test="${propiedadInstance?.nombre}">
					<li class="fieldcontain">
						<span id="nombre-label" class="property-label"><g:message code="propiedad.nombre.label" default="Nombre" /></span>
						
						<span class="property-value" aria-labelledby="nombre-label"><g:fieldValue bean="${propiedadInstance}" field="nombre"/></span>	
					</li>
				</g:if>																																												
			</ol>
						
			<g:render template="/multimediaImagen/showImagenPrincipal" bean="${propiedadInstance}" var="contenedor" />
			
			<g:render template="/multimediaImagen/showImagenesYPlanos" bean="${propiedadInstance}" var="contenedor" />
			
			<g:render template="/multimediaDocumento/showDocumentos" bean="${propiedadInstance}" var="contenedor" />	
			
			<g:render template="/multimediaVideo/showVideos" bean="${propiedadInstance}" var="contenedor" />						
			
			<g:form url="[resource:propiedadInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${propiedadInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>		
	</body>
</html>
