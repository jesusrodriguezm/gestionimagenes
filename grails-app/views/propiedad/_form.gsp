<%@ page import="gestionimagenes.Propiedad" %>


<div class="form-group">
	<div class="col-sm-2 control-label text-danger">
		<label for="nombre">
			<g:message code="propiedad.nombre.label" default="Nombre" />
			<span class="required-indicator">*</span>
		</label>
	</div>
	<div class="col-sm-10">
		<g:textField name="nombre" class="form-control" value="${propiedadInstance?.nombre}"/>
	</div>
</div>
 
<g:render template="/multimediaImagen/formImagenPrincipal" bean="${propiedadInstance}" var="contenedor" />

<g:render template="/multimediaImagen/formImagenes" bean="${propiedadInstance}" var="contenedor" />

<g:render template="/multimediaImagen/formPlanos" bean="${propiedadInstance}" var="contenedor" />

<g:render template="/multimediaDocumento/formDocumentos" bean="${propiedadInstance}" var="contenedor" />

<g:render template="/multimediaVideo/formVideos" bean="${propiedadInstance}" var="contenedor" />
 
