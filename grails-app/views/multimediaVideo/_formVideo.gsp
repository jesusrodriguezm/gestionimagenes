<div class="col-xs-12 col-sm-6 col-md-3">
	<div class="thumbnail">
		<g:if test="${i.direccionVideoValida()}">
			<div class="embed-responsive embed-responsive-16by9">
				<iframe class="embed-responsive-item" src="${ i.obtenerDireccionEmbeber() }" 
					frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			</div>
		</g:if>
		<g:else>
			El vídeo no está alojado ni en Youtube ni en Vimeo.
		</g:else>
		
		<div class="caption">
			<p>
				<span class="badge">
					${j}
				</span>
				<a href="${i.urlVideo}" target="_blank" title="${message(code:'multimediaVideo.verOrigen.label')}">${i.nombre }</a>
				
			</p>
			<p class="text-center">
				<small>
					<g:formatDate format="yyyy-MM-dd HH:mm:ss a" date="${i.dateCreated}" />
				</small>
			</p>

			<g:link controller="${i.getClass().simpleName}" action="delete" id="${i.id}"
				class="btn btn-xs btn-danger pull-right"
				onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">
				<span class="glyphicon glyphicon-remove"></span>Eliminar
     										</g:link>
			<br>
		</div>
	</div>
</div>