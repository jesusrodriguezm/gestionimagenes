<g:if test="${contenedor.id}">
	<g:if test="${flash.messageImagenPrincipal}">
		<div class="row">
			<div class="col-md-12">		
				<bootstrap:alert class="alert alert-info">${flash.messageImagenPrincipal}</bootstrap:alert>
			</div>
		</div>
	</g:if>
	
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<div class="col-sm-2 control-label">
					<label for="imagenPrincipalForm"> <g:message code="contenedor.imagenPrincipal.label"
							default="Imagen Principal" />
					</label>
				</div>
				<div class="col-sm-7">
					<input type="file" name="imagenPrincipalForm" id="imagenPrincipalForm" accept="image/png, image/jpg, image/jpeg"
						onchange="validarFormatoImagenes(this);$('#imagenPrincipalFormVisible').val($(this).val().split('\\').pop());"
						class="hidden" />

					<div class="input-group">
						<span class="input-group-btn"> <a class="btn btn-default"
							onclick="$('#imagenPrincipalForm').click();" title="Seleccionar imagen principal">Sel.</a>
						</span> 
						<input type="text" name="imagenPrincipalFormVisible" id="imagenPrincipalFormVisible"
							class="form-control" disabled /> 
						<span class="input-group-btn"> <g:actionSubmit
								value="${message(code:'default.add.label', args: [' '])}" action="addImagenPrincipal"
								class="btn btn-primary"
								title="${message(code:'default.add.label', args: [message(code: 'multimediaImagenPrincipal.label', default: 'Imagen principal')])}" />
						</span>
					</div>
					<p class="help-block">
						<g:message code="multimediaImagenPrincipal.ayudaSubida.label" default="Ficheros .jpg o .png de un máximo de 512KB" />												
					</p>
				</div>
				<div class="col-sm-3">
					<g:if test="${contenedor.imagenPrincipal}">
						<div class="thumbnail">
							<img
								src="<g:createLink controller="multimediaImagen" action="viewImage" 
							params="[id:"${contenedor.imagenPrincipal.id}" ]"/>"
							class="img-responsive img-rounded"/>
							<div class="caption text-center">
								<small><g:message code="contenedor.imagenPrincipal.label" default="Imagen Principal" /></small>
								<g:link controller="${contenedor.imagenPrincipal.getClass().simpleName}" action="delete"
									id="${contenedor.imagenPrincipal.id}" class="btn btn-xs btn-danger pull-right"
									onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">
									<span class="glyphicon glyphicon-remove"></span>
     							Eliminar
     					</g:link>
							</div>
						</div>
					</g:if>
				</div>
			</div>
		</div>
	</div>
</g:if>