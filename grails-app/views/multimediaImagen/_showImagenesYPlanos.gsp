<g:if test="${contenedor?.imagenes || contenedor?.planos}">
	<div class="panel panel-default">
		<div class="panel-heading">
			<g:message code="multimediaImagenes.label" default="Imagenes" />
			<span class="badge">${contenedor?.imagenes?.size() + contenedor?.planos?.size()}</span>			
		</div>
		<div class="panel-body text-center">			
			<div class="coverimg">
				<g:if test="${contenedor?.imagenes?.size()>0}">
					<h2><g:message code="multimediaImagenes.label" default="Imagenes" /></h2>
					<g:each in="${contenedor?.imagenes?.sort{it.id}}" var="i">
						<a href="#" onclick="e.preventDefault();"> <img data-gallery="my-gallery" class="imagenSobra img-rounded"
							src="<g:createLink controller='multimediaImagen' action='viewImage' params='[id:" ${i.id} "]' />"
							width="150px"/>
						</a>
					</g:each>
				</g:if>
				
				<g:if test="${contenedor?.planos?.size()>0}">
					<h2><g:message code="multimediaPlanos.label" default="Planos" /></h2>
					<g:each in="${contenedor?.planos?.sort{it.id}}" var="i">
						<a href="#" onclick="e.preventDefault();"> <img data-gallery="my-gallery" class="imagenSobra img-rounded"
							src="<g:createLink controller='multimediaImagen' action='viewImage' params='[id:" ${i.id} "]' />"
							width="150px"/>
						</a>
					</g:each>
				</g:if>
			</div>
		</div>
	</div>
	
	<script>
		inicializarGaleriaImagenes();
	</script>
</g:if>

