<g:if test="${contenedor.id}">
	<div class="panel panel-default" id="panelPlanos">
		<div class="panel-heading">
			<g:message code="multimediaPlanos.label" default="Planos" />
			<span class="badge">
				${contenedor?.planos?.size()}
			</span> <a data-toggle="collapse" data-parent="#accordion" href="#addPlano"
				class="btn btn-default btn-xs pull-right"
				title="${message(code:'default.add.label', args: [message(code: 'multimediaPlano.label', default: 'Plano')])}">
				<i class="glyphicon glyphicon-plus"></i>
			</a>
		</div>
		<div class="panel-body">
			<g:if test="${flash.messagePlano}">
				<bootstrap:alert class="alert alert-info">
					${flash.messagePlano}
				</bootstrap:alert>
			</g:if>

			<div id="addPlano" class="panel-collapse collapse">
				<g:if test="${contenedor.puedeAñadirNuevoPlano()}">
					<div class="row">
						<div class="col-md-12">							
							<div class="well well-sm">
								<div class="form-group">
									<div class="col-sm-3 control-label">
										<label> ${message(code: 'default.add.label', args: [message(code: 'multimediaPlano.label', default: 'Plano')])}
										</label>
									</div>
									<div class="col-sm-9">
										<input type="file" name="planoForm" id="planoForm" accept="image/png, image/jpg, image/jpeg"
											onchange="validarFormatoImagenes(this);$('#planoFormVisible').val($(this).val().split('\\').pop());"
											class="hidden" />

										<div class="input-group">
											<span class="input-group-btn"> <a class="btn btn-default" onclick="$('#planoForm').click();"
												title="Seleccionar nuevo plano">Sel.</a>
											</span> <input type="text" name="planoFormVisible" id="planoFormVisible" class="form-control" disabled />
											<span class="input-group-btn"> <g:actionSubmit
													value="${message(code:'default.add.label', args: [' '])}" action="addPlano"
													class="btn btn-primary"
													title="${message(code:'default.add.label', args: [message(code: 'multimediaPlano.label', default: 'Plano')])}" />
											</span>
										</div>

										<p class="help-block">
											<g:message code="multimediaPlanos.ayudaSubida.label"
												default="Ficheros .jpg o .png de un máximo de 512KB" />
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</g:if>
				<g:else>
					<div class="well well-sm">
						<g:message code="multimediaPlanos.maximoAlcanzado.label" default="Máximo de planos alcanzado" />
					</div>
				</g:else>
			</div>
				
			<g:if test="${contenedor?.planos?.size()>0}">
				<div class="row">
					<% int j = 1; %>
					<g:each in="${contenedor?.planos?.sort{it.id}}" var="i">
						<div class="col-xs-6 col-sm-4 col-md-3">						
							<div class="thumbnail">																
								<img src="<g:createLink controller='multimediaImagen' action='viewImage' params='[id:"${i.id}"]' />"
									 class="img-responsive  img-rounded" alt="Imagen" /> 								
								<div class="caption">
									<p><span class="badge">${j} <% j++; %></span>${i.nombre }</p>
									<p class="text-center">
										<small>${i.tamanoKB }KB</small><br> 
										<small><g:formatDate format="yyyy-MM-dd HH:mm:ss a" date="${i.dateCreated}" /></small>
									</p>

									<g:link controller="${i.getClass().simpleName}" action="delete" id="${i.id}"
										class="btn btn-xs btn-danger pull-right"
										onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">
										<span class="glyphicon glyphicon-remove"></span>Eliminar
			     					</g:link>
									<br>
								</div>
							</div>
						</div>
					</g:each>
				</div>
			</g:if>
		</div>
	</div>
</g:if>