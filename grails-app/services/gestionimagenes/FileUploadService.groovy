package gestionimagenes

import gestionimagenes.Propiedad
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.commons.CommonsMultipartFile
//import org.codehaus.groovy.grails.web.context.ServletContextHolder
import org.imgscalr.Scalr
import java.awt.image.BufferedImage
import javax.imageio.ImageIO

//import grails.transaction.Transactional

//@Transactional
class FileUploadService {
	
	boolean transactional = true
	
	def String obtenerRutaAbsolutaMultimedia() {
		def grailsApplication = new Propiedad().domainClass.grailsApplication
		def config = grailsApplication.config
		return config.multimedia.root.dir
	}
	
	private crearDirectorio(String storagePath) {
		// Create storage path directory if it does not exist
		def storagePathDirectory = new File(storagePath)
		if (!storagePathDirectory.exists()) {
			print "CREATING DIRECTORY ${storagePath}: "
			if (storagePathDirectory.mkdirs()) {
				println "SUCCESS"
			} else {
				println "FAILED"
			}
		}
	}
	
	//guarda la imagen, primero la redimensiona
	def String uploadImagenEscalada(MultipartFile file, String name, String destinationDirectory) {
		println "uploadImagenEscalada()"
		final int maxWidthImagen = 800
		final int maxHeightImagen = 600
		
		def directorioRoot = obtenerRutaAbsolutaMultimedia()
		
		//Se escala la imagen:		
		InputStream inputStream = file.getInputStream();
		BufferedImage bufferedImage = ImageIO.read(inputStream);
		BufferedImage scaledImage = Scalr.resize(bufferedImage, Scalr.Method.BALANCED, Scalr.Mode.FIT_TO_WIDTH, maxWidthImagen, maxHeightImagen, Scalr.OP_ANTIALIAS);
	
		def storagePath = directorioRoot + destinationDirectory		
		crearDirectorio(storagePath)
				
		// Store file
		if (!file.isEmpty()) {
			File outputfile = new File("${storagePath}/${name}");
			ImageIO.write(scaledImage, "png", outputfile);			
			println "Saved image: ${storagePath}${name}"
			return "${name}"

		} else {
			println "File ${file.inspect()} was empty!"
			return null
		}				
	}
	
	//guarda el fichero (se puede utilizar para imágenes pero no las va redimiensionar)
	def String uploadFile(MultipartFile file, String name, String destinationDirectory) {
		
		def directorioRoot = obtenerRutaAbsolutaMultimedia()
		
		def storagePath = directorioRoot + destinationDirectory
		crearDirectorio(storagePath)

        // Store file
        if (!file.isEmpty()) {
            file.transferTo(new File("${storagePath}/${name}"))
            println "Saved file: ${storagePath}${name}"
            //return "${storagePath}/${name}"
			return "${name}"

        } else {
            println "File ${file.inspect()} was empty!"
            return null
        }
    }
	
	def boolean removeFile(String file, String destinationDirectory) {		
		def directorioRoot = obtenerRutaAbsolutaMultimedia()
		def storagePath = directorioRoot + destinationDirectory
		println "Eliminando fichero: " + storagePath + file
		boolean fileSuccessfullyDeleted =  new File(storagePath + file).delete()
		println "Fichero eliminado: " + fileSuccessfullyDeleted
		return fileSuccessfullyDeleted
	}
	
	def String validarImagen(MultipartFile file) {
		return validarImagen(file, 0)
	}
	
	def String validarImagen(MultipartFile file, int sizeMax) {
		//sizeMax en KB
		String validacion = ""
		String contentType = file.getContentType();
		println "contentType: " + contentType
		if (contentType!="image/jpeg" && contentType!="image/png") {
			validacion = validacion + " Formato de imagen incorrecto (sólo se admiten .jpg y .png)"
		}
		
		if (sizeMax>0) { //sólo si el parámetro es mayor a 0
			double tamano = file.size
			tamano = ((tamano / 1024) as double).round() //para pasarlo a KB
			println "tamano: " + tamano
			
//			if (sizeMax == 0) {
//				sizeMax = 512 // 512KB
//			}
//			
			if (tamano > sizeMax) {
				validacion = validacion + " Tamaño de la imagen supera el máximo permitido ("+ sizeMax +"KB)"
			}
		}
		
		if (validacion != "") {
			validacion = "¡Error validación imagen: " + validacion + "!"
		}
		return validacion		
	}
	
	def String validarDocumento(MultipartFile file, int sizeMax) {
		//sizeMax en KB
		String validacion = ""
		String contentType = file.getContentType();
		println "contentType: " + contentType 
		if (contentType!="application/vnd.openxmlformats-officedocument.wordprocessingml.document"
			&& contentType!="application/msword"
			&& contentType!="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
			&& contentType!="application/octet-stream"
			&& contentType!="application/pdf" 
			&& contentType!="text/plain") {
			
			validacion = validacion + " Formato de imagen incorrecto (sólo se admiten documentos de Word, Excel, PDF o textos planos)"
		}
		double tamano = file.size
		tamano = ((tamano / 1024) as double).round() //para pasarlo a KB
		println "tamano: " + tamano
		
		if (sizeMax == 0) {
			sizeMax = 10240 // 10MB
		}
		
		if (tamano > sizeMax) {
			validacion = validacion + " Tamaño del documento supera el máximo permitido ("+ sizeMax +"KB)"
		}
		
		if (validacion != "") {
			validacion = "¡Error validación documento: " + validacion + "!"
		}
		return validacion
	}
}
