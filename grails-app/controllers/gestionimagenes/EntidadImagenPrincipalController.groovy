package gestionimagenes

import grails.transaction.Transactional;

@Transactional(readOnly = true)
class EntidadImagenPrincipalController {
	
	FileUploadService fileUploadService
	
		static allowedMethods = [create: ["GET", "POST"], delete: ["DELETE", "GET", "POST"]]
	   
		@Transactional
		def create() {
			println "EntidadImagenPrincipalController create"
			println "params.id: " + params.id
			
			Entidad entidadInstance = Entidad.get(params.id)
			
			// Save imagenPrincipal if uploaded
			def imagenPrincipal = request.imagenPrincipalForm // request.getFile('imagenPrincipalForm')
			def validarImagen = ""
			if (imagenPrincipal) {
				if (!imagenPrincipal.isEmpty()) {
					println "ACTUALIZANDO IMAGEN"
					//validarImagen = fileUploadService.validarImagen(imagenPrincipal, EntidadImagenPrincipal.sizeMax)
					validarImagen = fileUploadService.validarImagen(imagenPrincipal, 0)
					if (validarImagen == "") { //solo se inserta si se valida la imagen
						//Borrar la imagen anterior anterior
						if (entidadInstance.imagenPrincipal) {
							EntidadImagenPrincipal pipOld = entidadInstance.imagenPrincipal
							entidadInstance.imagenPrincipal=null;
							pipOld.delete(flush:true)
						}
						
						//Insertar la nueva imagen principal
						EntidadImagenPrincipal pip = new EntidadImagenPrincipal(imagenPrincipal,entidadInstance.id)
						pip.save(flush:true)						
						//pip.imagen = fileUploadService.uploadFile(imagenPrincipal, "${entidadInstance.id}-${pip.id}.png", pip.obtenerRuta())
						pip.imagen = fileUploadService.uploadImagenEscalada(imagenPrincipal, "${entidadInstance.id}-${pip.id}.png", pip.obtenerRuta())
						entidadInstance.imagenPrincipal = pip
						println "salvando entidadInstance con la imagen principal"
						entidadInstance.save(flush:true)
						
						flash.messageImagenPrincipal = message(code: 'default.created.message', args: [message(code: 'multimediaImagenPrincipal.label', default: 'Imagen principal'), pip.id])
					}
					flash.messageImagenPrincipal = flash.messageImagenPrincipal + ". " + validarImagen
				}
				else {
					flash.messageImagenPrincipal = "Debe seleccionar imagen principal"
				}
			}
			
			redirect controller:entidadInstance.getClass().simpleName, action:"edit", id:params.id, fragment:'panelImagenPrincipal'
		}
	
		   @Transactional
		def delete() {
			println "delete EntidadImagenPrincipalController nuevo: id:" + params.id
			def entidadImagenPrincipalInstance = EntidadImagenPrincipal.get(params.id)
			if (entidadImagenPrincipalInstance) {
				try {
					def controlladorDestino = entidadImagenPrincipalInstance.entidad.getClass().simpleName
					def idDestino = entidadImagenPrincipalInstance.entidad.id
					
					def entidad = entidadImagenPrincipalInstance.entidad
					//entidadImagenPrincipalInstance.delete(flush:true)
					entidadImagenPrincipalInstance.delete()
					entidad.imagenPrincipal = null
					entidad.save(flush:true)
					
					println "imagen principal entidad borrada"
					
					flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'multimediaImagenPrincipal.label', default: 'Imagen Principal'), entidadImagenPrincipalInstance])}"
					//redirect(action: "list")
					redirect controller:controlladorDestino, action:'edit', id:idDestino
				}
				catch (org.springframework.dao.DataIntegrityViolationException e) {
					log.error("Delete Exception: " + e)
					flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'multimediaImagenPrincipal.label', default: 'Imagen Principal'), params.id])}"
					redirect(action: "show", id: params.id)
				}
			}
			else {
				flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'multimediaImagenPrincipal.label', default: 'Imagen Principal'), params.id])}"
				redirect(action: "list")
			}
		}

    
}
