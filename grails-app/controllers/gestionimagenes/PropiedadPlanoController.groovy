package gestionimagenes

import grails.transaction.Transactional;

@Transactional(readOnly = true)
class PropiedadPlanoController {
	
	FileUploadService fileUploadService
	
	static allowedMethods = [create: ["GET", "POST"], delete: ["DELETE", "GET", "POST"]]
	
	@Transactional
	def create() {
		println "PropiedadPlanoController create"
		println "params.id: " + params.id
		
		if (params.id) {
			def plano = request.planoForm // request.getFile('planoForm')
			if (plano) {
				def validarImagen = ""
				if (!plano.isEmpty()) {
					println "VALIDANDO PLANO"
					//validarImagen = fileUploadService.validarImagen(plano, PropiedadPlano.sizeMax)
					validarImagen = fileUploadService.validarImagen(plano, 0)
					if (validarImagen == "") { //solo se inserta si se valida la imagen
						PropiedadPlano propiedadPlanoInstance = new PropiedadPlano(plano, (params.id) as Long)
						if(propiedadPlanoInstance.save(flush:true)) {
							// Save imagenPrincipal if uploaded
							println "CREANDO PLANO"
							//propiedadPlanoInstance.imagen = fileUploadService.uploadFile(plano, "${propiedadPlanoInstance.propiedad.id}-${propiedadPlanoInstance.id}.png", propiedadPlanoInstance.obtenerRuta())
							propiedadPlanoInstance.imagen = fileUploadService.uploadImagenEscalada(plano, "${propiedadPlanoInstance.propiedad.id}-${propiedadPlanoInstance.id}.png", propiedadPlanoInstance.obtenerRuta())							
							propiedadPlanoInstance.save(flush:true)
							println "PLANO GUARDADO"
							flash.messagePlano = message(code: 'default.created.message', args: [message(code: 'multimediaPlano.label', default: 'Plano'), propiedadPlanoInstance.id])
						}
					}
					flash.messagePlano = flash.messagePlano + ". " + validarImagen
				}
				else {
					flash.messagePlano = "Debe seleccionar un plano"
				}
								
			}
		}
		
		Propiedad pi = Propiedad.get(params.id)
		redirect controller:pi.getClass().simpleName, action:"edit", id:params.id, fragment:'panelPlanos'
	}

	@Transactional
	def delete() {
		println "delete propiedadPlanoController nuevo: id:" + params.id
		def propiedadPlanoInstance = PropiedadPlano.get(params.id)
		if (propiedadPlanoInstance) {
			try {
				def controlladorDestino = propiedadPlanoInstance.propiedad.getClass().simpleName
				def idDestino = propiedadPlanoInstance.propiedad.id
				
				propiedadPlanoInstance.delete(flush:true)
				println "plano borrado"
				
				flash.messagePlano = "${message(code: 'default.deleted.message', args: [message(code: 'multimediaPlano.label', default: 'Plano'), propiedadPlanoInstance])}"
				//redirect(action: "list")
				redirect controller:controlladorDestino, action:'edit', id:idDestino, fragment:'panelPlanos'
			}
			catch (org.springframework.dao.DataIntegrityViolationException e) {
				log.error("Delete Exception: " + e)
				flash.messagePlano = "${message(code: 'default.not.deleted.message', args: [message(code: 'multimediaPlano.label', default: 'Plano'), params.id])}"
				redirect(action: "show", id: params.id)
			}
		}
		else {
			flash.messagePlano = "${message(code: 'default.not.found.message', args: [message(code: 'multimediaPlano.label', default: 'Plano'), params.id])}"
			redirect(action: "list")
		}
	}
}
