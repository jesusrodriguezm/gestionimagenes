package gestionimagenes

import grails.transaction.Transactional;

@Transactional(readOnly = true)
class PropiedadImagenPrincipalController {

	FileUploadService fileUploadService

	static allowedMethods = [create: ["GET", "POST"], delete: ["DELETE", "GET", "POST"]]
   
	@Transactional
	def create() {
		println "PropiedadImagenPrincipalController create"
		println "params.id: " + params.id
		
		Propiedad propiedadInstance = Propiedad.get(params.id)
		
		// Save imagenPrincipal if uploaded
		def imagenPrincipal = request.imagenPrincipalForm // request.getFile('imagenPrincipalForm')
		def validarImagen = ""
		if (imagenPrincipal) {
			if (!imagenPrincipal.isEmpty()) {
				println "ACTUALIZANDO IMAGEN"
				//validarImagen = fileUploadService.validarImagen(imagenPrincipal, PropiedadImagenPrincipal.sizeMax)
				validarImagen = fileUploadService.validarImagen(imagenPrincipal, 0)
				if (validarImagen == "") { //solo se inserta si se valida la imagen
					//Borrar la imagen anterior anterior
					if (propiedadInstance.imagenPrincipal) {
						PropiedadImagenPrincipal pipOld = propiedadInstance.imagenPrincipal
						propiedadInstance.imagenPrincipal=null;
						pipOld.delete(flush:true)
					}
					
					//Insertar la nueva imagen principal
					PropiedadImagenPrincipal pip = new PropiedadImagenPrincipal(imagenPrincipal,propiedadInstance.id)
					pip.save(flush:true)
					//pip.imagen = fileUploadService.uploadFile(imagenPrincipal, "${propiedadInstance.id}-${pip.id}.png", pip.obtenerRuta())
					pip.imagen = fileUploadService.uploadImagenEscalada(imagenPrincipal, "${propiedadInstance.id}-${pip.id}.png", pip.obtenerRuta())					
					propiedadInstance.imagenPrincipal = pip
					println "salvando propiedadInstance con la imagen principal"
					propiedadInstance.save(flush:true)
					
					flash.messageImagenPrincipal = message(code: 'default.created.message', args: [message(code: 'multimediaImagenPrincipal.label', default: 'Imagen Principal'), pip.id])
				}
				flash.messageImagenPrincipal = flash.messageImagenPrincipal + ". " + validarImagen
			}
			else {
				flash.messageImagenPrincipal = "Debe seleccionar imagen principal"
			}
		}
		
		redirect controller:propiedadInstance.getClass().simpleName, action:"edit", id:params.id, fragment:'panelImagenPrincipal'
	}

   	@Transactional
	def delete() {
		println "delete PropiedadImagenPrincipalController nuevo: id:" + params.id
		def propiedadImagenPrincipalInstance = PropiedadImagenPrincipal.get(params.id)
		if (propiedadImagenPrincipalInstance) {
			try {
				def controlladorDestino = propiedadImagenPrincipalInstance.propiedad.getClass().simpleName
				def idDestino = propiedadImagenPrincipalInstance.propiedad.id
				
				def propiedad = propiedadImagenPrincipalInstance.propiedad				
				//propiedadImagenPrincipalInstance.delete(flush:true)
				propiedadImagenPrincipalInstance.delete()
				propiedad.imagenPrincipal = null
				propiedad.save(flush:true)
				
				println "imagen principal propiedad borrada"
				
				flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'multimediaImagenPrincipal.label', default: 'Imagen Principal'), propiedadImagenPrincipalInstance])}"
				//redirect(action: "list")
				redirect controller:controlladorDestino, action:'edit', id:idDestino
			}
			catch (org.springframework.dao.DataIntegrityViolationException e) {
				log.error("Delete Exception: " + e)
				flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'multimediaImagenPrincipal.label', default: 'Imagen Principal'), params.id])}"
				redirect(action: "show", id: params.id)
			}
		}
		else {
			flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'multimediaImagenPrincipal.label', default: 'Imagen Principal'), params.id])}"
			redirect(action: "list")
		}
	}
}
